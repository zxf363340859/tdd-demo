package com.twuc.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;


class GameTest {

    private Game game;
    private List<Integer>scoreList;
    @BeforeEach
    void setUp() {
        game = new Game();
        scoreList = new ArrayList<>();
    }

    @Test
    void should_return_zero_when_without_input() {
        assertEquals(0, game.getTotalScore());
    }

    @Test
    void should_return_number_of_knocked_down_pins_in_one_throw(){
        scoreList = Arrays.asList(1);
        game.calculateTotalScore(scoreList);
        assertEquals((int)scoreList.get(0),game.getTotalScore());
    }

    @Test
    void should_return_number_of_knocked_down_pins_when_throw_twice(){
        scoreList = Arrays.asList(1, 2);
        game.calculateTotalScore(scoreList);
        assertEquals(getSum(),game.getTotalScore());
    }

    @Test
    void should_return_number_of_knocked_down_pins_when_throw_10_frame(){
        scoreList = Arrays.asList(1, 2, 3, 4, 5, 4, 4, 3, 2, 1, 1, 2, 3, 4, 4, 5, 4, 3, 2, 1);
        game.calculateTotalScore(scoreList);
        assertEquals(getSum(),game.getTotalScore());
    }

    private int getSum() {
        return (int) scoreList.stream().reduce(Integer::sum).get();
    }

    @Test
    void should_return_with_spare_bonus_when_first_frame_is_spare(){
        scoreList = Arrays.asList(4, 6, 3, 4, 5, 4, 4, 3, 2, 1, 1, 2, 3, 4, 4, 5, 4, 3, 2, 1);
        game.calculateTotalScore(scoreList);
        assertEquals(getSum()+scoreList.get(2), game.getTotalScore());
    }

    @Test
    void should_return_with_spare_bonus_when_something_frame_is_spare_and_last_not_spare(){
        scoreList = Arrays.asList(1, 2, 5, 5, 3, 2, 4, 3, 2, 1, 1, 2, 3, 4, 2, 2, 4, 3, 2, 1);
        game.calculateTotalScore(scoreList);
        assertEquals(getSum()+scoreList.get(4), game.getTotalScore());
    }

    @Test
    void should_return_with_spare_bonus_when_something_frame_is_spare_and_last_spare(){
        scoreList = Arrays.asList(1, 2, 3, 4, 4, 5, 4, 3, 2, 1, 1, 2, 3, 4, 4, 5, 4, 3, 2, 8, 5);
        game.calculateTotalScore(scoreList);
        assertEquals(getSum() + scoreList.get(20), game.getTotalScore());
    }

    @Test
    void should_return_with_first_strike_bonus_when_10_frame(){
        scoreList = Arrays.asList(10, 0, 3, 4, 4, 4, 4, 3, 2, 1, 1, 2, 3, 4, 4, 2, 4, 3, 2, 8, 5);
        game.calculateTotalScore(scoreList);
        assertEquals(getSum()+ scoreList.get(2) + scoreList.get(3)+scoreList.get(20), game.getTotalScore());
    }

   @Test
    void should_return_with_strik_bonus_when_10_frame_and_something_strike_and_last_not_strike(){
        scoreList = Arrays.asList(10, 0, 3, 4, 4, 4, 4, 3, 2, 1, 1, 2, 3, 4, 4, 2, 4, 3, 2, 7);
        game.calculateTotalScore(scoreList);
        assertEquals(getSum()+ scoreList.get(2) + scoreList.get(3), game.getTotalScore());
    }

    @Test
    void should_return_with_strike_bonus_when_10_frame_and_last_strike(){
        scoreList = Arrays.asList(1, 4, 3, 4, 4, 4, 4, 3, 2, 1, 1, 2, 3, 4, 4, 2, 4, 3, 10, 0, 4, 5);
        game.calculateTotalScore(scoreList);
        assertEquals(getSum()+ scoreList.get(20), scoreList.get(21), game.getTotalScore());
    }

    @Test
    void should_return_with_strik_bonus_when_10_frame_strike(){
        List<Integer> scoreList = new ArrayList<>();
        while (scoreList.size() < 20) {
            scoreList.add(10);
            scoreList.add(0);
        }
        scoreList.add(10);
        scoreList.add(10);
        game.calculateTotalScore(scoreList);
        assertEquals(300, game.getTotalScore());
    }


}
