package com.twuc.game;

import java.util.List;

public class Game {
    private int totalScore;
    public int getTotalScore(){
        return totalScore;
    }

    public void calculateTotalScore(List<Integer> scores) {
        if (scores.size() == 0)
            return;
        int tempScore = 0;
        for (int index = 0; index < scores.size() && index < 20; index++) {
            if (isStrike(scores, index)) {
                tempScore += getNextPositiveScoreSum(scores, index);
            } else if (isSpare(scores,index)) {
                tempScore += scores.get(index + 1);
                if(lastFrameIsSpare(index)){
                    tempScore += scores.get(index+1);
                }
            }
            tempScore += scores.get(index);
        }

        totalScore=tempScore;
    }

    private int getNextPositiveScoreSum(List<Integer> scores,  int index) {
        int count = 2;
        int nextIndex = index + 1;
        int tempScore=0;
        while (count > 0 && nextIndex < scores.size()) {
            if (scores.get(nextIndex) > 0) {
                count--;
                tempScore += scores.get(nextIndex);
            }
            nextIndex++;
        }
        return tempScore;
    }

    private boolean isStrike(List<Integer> scores, int index) {
        return index % 2 == 0 && scores.get(index) == 10;
    }

    private boolean lastFrameIsSpare(int index) {
        return index==19;
    }

    private boolean isSpare(List<Integer> scores, int index) {
        if(index%2==1 && scores.get(index)+scores.get(index-1)==10 && scores.get(index-1)!=10){
            return true;
        }
        return false;
    }

}
